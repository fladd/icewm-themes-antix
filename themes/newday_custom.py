#!/usr/bin/env python3

"""newday_custom

Create a custom NewDay theme.

"""

__author__ = "fladd"
__version__ = "0.5.0"
__date__ = "2023-04-24"


import os
import sys
import glob
import shutil
import argparse


# Helper functions
def get_fg_color(color):
    rgb = list(int(color[i:i+2], 16) for i in (0, 2, 4))
    ys = (rgb[0] / 255.0) ** 2.2 * 0.2126 + \
         (rgb[1] / 255.0) ** 2.2 * 0.7152 + \
         (rgb[2] / 255.0) ** 2.2 * 0.0722
    if ys ** 0.678 < 0.5:
        return "FFFFFF"
    else:
        return "000000"

# Parse arguments
parser = argparse.ArgumentParser(
        description="Create a custom NewDay theme.")

parser.add_argument(
        "-V", "--version",
        action="version",
        version=__version__,
        help="show version information and exit")
parser.add_argument(
        "-a", "--application-icon",
        action="store_true",
        help="show application icon in window titlebar")
parser.add_argument(
        "-c", "--color",
        required=False,
        help="the accent color as 6-digit HEX string (default='5294E2')",
        default="5294E2")
parser.add_argument(
        "-d", "--desktop-icon",
        choices=["desktop", "minimize", "windows"],
        default="windows",
        help="the 'desktop' icon to use (default='windows')")
parser.add_argument(
        "-l", "--look",
        choices=["3d", "flat"],
        default="flat",
        help="the theme look (default='flat')")
parser.add_argument(
        "-m", "--mode",
        choices=["dark", "light"],
        default="dark",
        help="the theme mode (default='dark')")
parser.add_argument(
        "-o", "--output-directory",
        required=False,
        help="the directory to save the custom theme to \n" +\
              "(default='~/.icewm/themes/NewDay-Custom')")
parser.add_argument(
        "-s", "--start-icon",
        choices=["thick", "thick-inverted", "thin", "thin-inverted"],
        default="thin",
        help="the 'start' icon to use (default='thin')")
parser.add_argument(
        "-t", "--taskbuttonactive-style",
        choices=["brighter", "color", "darker"],
        default="darker",
        help="the 'taskbuttonactive' style to use (default='darker`')")
parser.add_argument(
        "-v", "--variant",
        choices=["huge", "large", "medium", "small"],
        default="medium",
        help="the theme variant to customize (default='medium')")
parser.add_argument(
        "-w", "--workspacebuttonactive-style",
        choices=["color", "grey"],
        default="grey",
        help="the 'workspacebuttonactive' style to use (default='grey')")
args = parser.parse_args()

if len(args.color) != 6:
    parser.print_help()
    sys.exit()
for x in args.color.upper():
    if x not in "0123456789ABCDEF":
        parser.print_help()
        sys.exit()

# Get source and output directory
source_dir = os.path.join(os.path.abspath(os.path.split(__file__)[0]),
                          f"NewDay-{args.variant.capitalize()}")

if args.output_directory is None:
    output_dir = os.path.expanduser("~/.icewm/themes/NewDay-Custom")
else:
    output_dir = os.path.abspath(args.output_directory)

# Copy theme files
shutil.copytree(source_dir, output_dir, dirs_exist_ok=True)

# Make changes in theme files
files = ("default.theme",
         "closeA.xpm",
         "maximizeA.xpm",
         "menuButtonA.xpm",
         "menusel.xpm",
         "minimizeA.xpm",
         "restoreA.xpm",
         "titleAB.xpm",
         "titleAS.xpm",
         "titleAT.xpm",
         "taskbar/start.xpm",
         "taskbar/start_alternative_thick.xpm",
         "taskbar/taskbuttonactive.xpm",
         "taskbar/workspacebuttonactive.xpm")

for file in files:
    with open(os.path.join(output_dir, file)) as f:
        file_content = f.read()
        
    # Replace accent color
    file_content = file_content.replace('5294E2', args.color)

    # Set alterantive styles if requested
    if file == "taskbar/taskbuttonactive.xpm":
        if (args.taskbuttonactive_style == "brighter" and \
            args.mode == "dark") or \
            (args.taskbuttonactive_style == "darker" and \
            args.mode == "light"):
                file_content = file_content.replace('#272824', '#585955')
        elif args.taskbuttonactive_style == "color":
            file_content = file_content.replace("#272824", f"#{args.color}")

    if file == "taskbar/workspacebuttonactive.xpm":
        if args.workspacebuttonactive_style == "color":
            file_content = file_content.replace("#A0A0A3", f"#{args.color}")


    # Set 3D look and application icon if requested
    if file == "default.theme":
        if args.look == "3d":
            file_content = file_content.replace(
                'Look=flat', 'Look=metal')
            file_content = file_content.replace(
                'BorderSizeX=1', 'BorderSizeX=2')
            file_content = file_content.replace(
                'BorderSizeY=1', 'BorderSizeY=2')
            file_content = file_content.replace(
                'DlgBorderSizeX=1', 'DlgBorderSizeX=2')
            file_content = file_content.replace(
                'DlgBorderSizeY=1', 'DlgBorderSizeY=2')
            file_content = file_content.replace(
                'PagerShowBorders=0', 'PagerShowBorders=1')

        if args.application_icon:
            file_content = file_content.replace(
                    'ShowMenuButtonIcon=0', 'ShowMenuButtonIcon=1')

    with open(os.path.join(output_dir, file), 'w') as f:
        f.write(file_content)

# Set alternative icons if requested
if args.desktop_icon != "windows":
    shutil.copyfile(os.path.join(
        output_dir,
        f"taskbar/desktop_alternative_{args.desktop_icon}.xpm"),
                    os.path.join(output_dir, "taskbar/desktop.xpm"))

if not args.start_icon.startswith("thin"):
    shutil.move(os.path.join(
        output_dir,
        f"taskbar/start_alternative_{args.start_icon.split('-')[0]}.xpm"),
                os.path.join(output_dir, "taskbar/start.xpm"))

if args.start_icon.endswith("inverted"):
    with open(os.path.join(output_dir, f"taskbar/start.xpm")) as f:
        file_content = f.read()

    file_content = file_content.replace(f"#{args.color}", "TEMP")
    file_content = file_content.replace("None", f"#{args.color}")
    file_content = file_content.replace("TEMP", "None")

    with open(os.path.join(output_dir, f"taskbar/start.xpm"), 'w') as f:
        f.write(file_content)

# Remove unused files
alternative_files = glob.glob(os.path.join(output_dir,
                                           "taskbar/*_alternative_*.xpm"))
for file in alternative_files:
    os.remove(file)
os.remove(os.path.join(output_dir, "taskbar/how-to-use.txt"))

# Handle light mode
if args.mode == "light":
    files = ("default.theme",
             "closeI.xpm",
             "closeO.xpm",
             "maximizeI.xpm",
             "maximizeO.xpm",
             "menuButtonI.xpm",
             "minimizeI.xpm",
             "minimizeO.xpm",
             "restoreI.xpm",
             "restoreO.xpm",
             "titleIB.xpm",
             "titleIS.xpm",
             "titleIT.xpm",
             "taskbar/collapse.xpm",
             "taskbar/desktop.xpm",
             "taskbar/expand.xpm",
             "taskbar/taskbuttonactive.xpm",
             "taskbar/windows.xpm",
             "taskbar/workspacebuttonactive.xpm",
             "taskbar/workspacebuttonbg.xpm")

    for file in files:
        # Replace colors
        with open(os.path.join(output_dir, file)) as f:
            file_content = f.read()
        file_content = file_content.replace('3B3C37', 'C4C3C8')
        file_content = file_content.replace('807D78', '7F8287')
        file_content = file_content.replace('DFDBD2', '20242D')
        file_content = file_content.replace('7A7A7A', '858585')
        file_content = file_content.replace('3C3B37', 'C3C4C8')
        file_content = file_content.replace('272824', 'D8D7DB')
        file_content = file_content.replace('585955', 'a7a6aa')

        if file[:3] in ("max", "min", "res"):
            file_content = file_content.replace('FFFFFF', '000000')

        if file == "default.theme":
            if args.taskbuttonactive_style != "color":
                file_content = file_content.replace(
                            'ColorActiveTaskBarAppText="#FFFFFF"',
                            'ColorActiveTaskBarAppText="#000000"')
            file_content = file_content.replace(
                    'ColorNormalWorkspaceButton="#C4C3C8"',
                    'ColorNormalWorkspaceButton="#FBFAFF"')
            file_content = file_content.replace(
                    'ColorToolTip="#000000"',
                    'ColorToolTip="#FFFFFF"')
            file_content = file_content.replace(
                    'ColorToolTipText="#FFFFFF"',
                    'ColorToolTipText="#000000"')
            file_content = file_content.replace(
                    'ColorMoveSizeStatus="#000000"',
                    'ColorMoveSizeStatus="#FFFFFF"')
            file_content = file_content.replace(
                    'ColorMoveSizeStatusText="#FFFFFF"',
                    'ColorMoveSizeStatusText="#000000"')

        with open(os.path.join(output_dir, file), 'w') as f:
            f.write(file_content)


# Set text color based on accent color
text_color = get_fg_color(args.color)

files = ("default.theme",
         "maximizeA.xpm",
         "minimizeA.xpm",
         "restoreA.xpm")

for file in files:
    with open(os.path.join(output_dir, file)) as f:
        file_content = []
        for line in f:
            if file == "default.theme":
                settings = ['ColorActiveTitleBarText',
                            'ColorActiveMenuItemText',
                            'ColorActiveWorkspaceButtonText',
                            'ColorInputSelectionText',
                            'ColorListBoxSelectionText',
                            'ColorQuickSwitchSelectionText']
                if args.taskbuttonactive_style == "color":
                    settings.append('ColorActiveTaskBarAppText=')
                if args.workspacebuttonactive_style == "color":
                    settings.append('ColorActiveWorkspaceButtonText=')

                if line.startswith(tuple(settings)):
                    setting = line[:line.index("=")]
                    file_content.append(f'{setting}="#{text_color}"\n')
                else:
                    file_content.append(line)

            if file in ("maximizeA.xpm", "minimizeA.xpm", "restoreA.xpm"):
                if line.startswith('"+\tc'):
                    file_content.append(f'"+\tc #{text_color}",\n')
                else:
                    file_content.append(line)
        
    with open(os.path.join(output_dir, file), 'w') as f:
        f.write("".join(file_content))
        
# Give some feedback to user upon success
print(f"Custom theme saved to '{output_dir}'")
